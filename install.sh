echo "installing dependencies via pacman"
sudo pacman -S tigervnc
echo "setting vnc password"
vncpasswd
echo "copying service to user service folder"
mkdir -p ~/.config/systemd/user/
cp x0vncserver.service ~/.config/systemd/user/x0vncserver.service
echo "activating service at boot"
systemctl --user daemon-reload
systemctl --user enable x0vncserver.service
systemctl --user start x0vncserver.service
