# systemd-user-x0vncserver

## Provides user service to x0vncserver.service

## VNC


```
sudo pacman -S tigervnc
```

```
vncpasswd
```

```
mkdir -p ~/.config/systemd/user/
```
```
~/.config/systemd/user/x0vncserver.service
```

```
[Unit]
Description=Remote desktop service (VNC)

[Service]
Type=simple
ExecStartPre=/bin/sh -c 'while ! pgrep Xorg; do sleep 2; done'
ExecStart=/usr/bin/x0vncserver -display :0 -rfbauth %h/.vnc/passwd

[Install]
WantedBy=default.target

```

```
systemctl --user daemon-reload  && systemctl --user enable x0vncserver.service  && systemctl --user  start x0vncserver.service

```
